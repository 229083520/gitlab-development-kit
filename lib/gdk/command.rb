# frozen_string_literal: true

require_relative 'command/diff_config'
require_relative 'command/doctor'
require_relative 'command/help'

module GDK
  module Command
  end
end
